package models;

import models.playables.CopyCatMoveBehaviour;
import models.playables.GrudgerMoveBehaviour;
import org.junit.Assert;
import org.junit.Test;

import java.util.Observer;

import static org.mockito.Mockito.mock;

public class GrudgerMoveBehaviourTest {

    @Test
    public void shouldReturnInstanceOfCopyCatPlayer() {
        GrudgerMoveBehaviour behaviour = new GrudgerMoveBehaviour();
        Assert.assertNotNull(behaviour);
    }

    @Test
    public void shouldReturnCooperateAsFirstMove() {
        GrudgerMoveBehaviour behaviour = new GrudgerMoveBehaviour();
        Assert.assertEquals(Move.COOPERATE, behaviour.getCurrentMove());
    }

    @Test
    public void shouldReturnAlwaysCheatWhenUpdateSendCheat() {
       GrudgerMoveBehaviour behaviour = new GrudgerMoveBehaviour();
       Game game = mock(Game.class);
       behaviour.update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
       Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());
        behaviour.update(game, new Move[]{Move.CHEAT, Move.CHEAT});
        Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());

        behaviour.update(game, new Move[]{Move.CHEAT, Move.COOPERATE});
        Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());
    }
}
