package models;

import models.playables.CopyCatMoveBehaviour;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.mock;

public class CopyCatMoveBehaviourTest {

    @Test
    public void shouldReturnInstanceOfCopyCatPlayer() {
        CopyCatMoveBehaviour player = new CopyCatMoveBehaviour();
        Assert.assertNotNull(player);
    }

    @Test
    public void shouldReturnCooperateAsFirstMove() {
        CopyCatMoveBehaviour player = new CopyCatMoveBehaviour();
        Assert.assertEquals(Move.COOPERATE, player.getCurrentMove());
    }

    @Test
    public void shouldReturnIOpponentsMoveAsNextMove() {
       CopyCatMoveBehaviour behaviour = new CopyCatMoveBehaviour();
       Game game = mock(Game.class);
       behaviour.update(game, new Move[]{Move.CHEAT, Move.COOPERATE});
       Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());
    }
}
