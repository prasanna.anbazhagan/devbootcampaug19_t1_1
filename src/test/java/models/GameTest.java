package models;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class GameTest {

    private Player player1;
    private Player player2;
    private Machine machine;
    private GameOutput gameOutput;

    @Before
    public void setUp() {
        player1 = Mockito.mock(Player.class);
        player2 = Mockito.mock(Player.class);
        machine = Mockito.mock(Machine.class);
        gameOutput = Mockito.mock(GameOutput.class);
    }
    @Test
    public void shouldPlayOneGame() {

        Mockito.when(player1.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(player2.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(machine.calculateScore(Move.COOPERATE, Move.COOPERATE)).thenReturn(new Scores(2,2));

        Game game = new Game(player1, player2, machine, 1, gameOutput);
        game.play();

        Mockito.verify(player1).getCurrentMove();
        Mockito.verify(player2).getCurrentMove();
        Mockito.verify(machine).calculateScore(player1.getCurrentMove(), player2.getCurrentMove());
    }

    @Test
    public void playingOneRoundShouldSetCorrectScoreForEachPlayer() {
        Mockito.when(player1.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(player2.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(machine.calculateScore(player1.getCurrentMove(), player2.getCurrentMove())).thenReturn(new Scores(2,2));

        Game game = new Game(player1, player2, machine, 1, gameOutput);
        game.play();
        Mockito.verify(player1).addScore(2);
        Mockito.verify(player2).addScore(2);
    }

    @Test
    public void playingMultipleRoundShouldSetCorrectScoreForEachPlayer() {
        Mockito.when(player1.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(player2.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(machine.calculateScore(player1.getCurrentMove(), player2.getCurrentMove())).thenReturn(new Scores(2,2));

        Game game = new Game(player1, player2, machine, 2, gameOutput);
        game.play();

        Mockito.verify(player1, Mockito.times(2)).addScore(2);
        Mockito.verify(player2, Mockito.times(2)).addScore(2);
    }

    @Test
    public void shouldOutputTheGoodPlayerBadPlayerTableOneRounds() {
        Mockito.when(player1.getCurrentMove()).thenReturn(Move.COOPERATE);
        Mockito.when(player2.getCurrentMove()).thenReturn(Move.CHEAT);
        Mockito.when(player1.getScore()).thenReturn(-1);
        Mockito.when(player2.getScore()).thenReturn(3);
        Mockito.when(machine.calculateScore(player1.getCurrentMove(),
                player2.getCurrentMove())).thenReturn(new Scores(-1,3));

        Game game = new Game(player1, player2, machine, 1, gameOutput);
        game.play();

        Mockito.verify(gameOutput).printString("1\t-1\t3");
    }

    @Test
    public void shouldOutputTheGoodPlayerBadPlayerTable5Rounds() {
        Mockito.when(player1.getCurrentMove()).thenReturn(Move.COOPERATE)
                .thenReturn(Move.COOPERATE).thenReturn(Move.COOPERATE).thenReturn(Move.COOPERATE).thenReturn(Move.COOPERATE);
        Mockito.when(player2.getCurrentMove()).thenReturn(Move.CHEAT)
                .thenReturn(Move.CHEAT).thenReturn(Move.CHEAT).thenReturn(Move.CHEAT).thenReturn(Move.CHEAT);
        Mockito.when(player1.getScore()).thenReturn(-1).thenReturn(-2).thenReturn(-3).thenReturn(-4).thenReturn(-5);
        Mockito.when(player2.getScore()).thenReturn(3).thenReturn(6).thenReturn(9).thenReturn(12).thenReturn(15);
        Mockito.when(machine.calculateScore(player1.getCurrentMove(), player2.getCurrentMove())).
                thenReturn(new Scores(-1,3))
                .thenReturn(new Scores(-1,3))
                .thenReturn(new Scores(-1,3))
                .thenReturn(new Scores(-1, 3))
                .thenReturn(new Scores(-1,3));


        Game game = new Game(player1, player2, machine, 5, gameOutput);
        game.play();

        Mockito.verify(gameOutput).printString("1\t-1\t3");
        Mockito.verify(gameOutput).printString("2\t-2\t6");
        Mockito.verify(gameOutput).printString("3\t-3\t9");
        Mockito.verify(gameOutput).printString("4\t-4\t12");
        Mockito.verify(gameOutput).printString("5\t-5\t15");
    }
}
