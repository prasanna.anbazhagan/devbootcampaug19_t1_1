package models;

import models.playables.CopyCatMoveBehaviour;
import models.playables.DetectiveBehaviour;
import models.playables.GrudgerMoveBehaviour;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.PrintStream;

public class GameIntegrationTest {

    @Test
    public void shouldPrintGameScoresForACheaterAndCopyCatFor5Rounds() {
        PrintStream printStream = Mockito.mock(PrintStream.class);
        Player cheater = new Player(() -> Move.CHEAT);
        CopyCatMoveBehaviour copyCatMoveBehaviour = new CopyCatMoveBehaviour();
        Player copyCat = new Player(copyCatMoveBehaviour);
        Game game = new Game(cheater, copyCat, new Machine(), 5, output -> printStream.print(output));
        game.addObserver(copyCatMoveBehaviour);
        game.play();

        Mockito.verify(printStream).print("1\t3\t-1");
        Mockito.verify(printStream).print("2\t3\t-1");
        Mockito.verify(printStream).print("3\t3\t-1");
        Mockito.verify(printStream).print("4\t3\t-1");
        Mockito.verify(printStream).print("5\t3\t-1");
    }

    @Test
    public void shouldPrintGameScoresForACopyCatAndGrudgerFor5Rounds() {
        PrintStream printStream = Mockito.mock(PrintStream.class);

        GrudgerMoveBehaviour grudgerMoveBehaviour = new GrudgerMoveBehaviour();
        Player grudger = new Player(grudgerMoveBehaviour);
        CopyCatMoveBehaviour copyCatMoveBehaviour = new CopyCatMoveBehaviour();
        Player copyCat = new Player(copyCatMoveBehaviour);
        Game game = new Game(grudger, copyCat, new Machine(), 5, output -> printStream.print(output));
        game.addObserver(copyCatMoveBehaviour);
        game.addObserver(grudgerMoveBehaviour);
        game.play();

        Mockito.verify(printStream).print("1\t2\t2");
        Mockito.verify(printStream).print("2\t4\t4");
        Mockito.verify(printStream).print("3\t6\t6");
        Mockito.verify(printStream).print("4\t8\t8");
        Mockito.verify(printStream).print("5\t10\t10");
    }

    @Test
    public void shouldPrintGameScoresForACopyCatAndDetectiveFor5Rounds() {
        PrintStream printStream = Mockito.mock(PrintStream.class);

        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour();
        Player detective = new Player(detectiveBehaviour);
        CopyCatMoveBehaviour copyCatMoveBehaviour = new CopyCatMoveBehaviour();
        Player copyCat = new Player(copyCatMoveBehaviour);
        Game game = new Game(copyCat, detective, new Machine(), 5, output -> printStream.print(output));
        game.addObserver(copyCatMoveBehaviour);
        game.addObserver(detectiveBehaviour);
        game.play();

        Mockito.verify(printStream).print("1\t2\t2");
        Mockito.verify(printStream).print("2\t1\t5");
        Mockito.verify(printStream).print("3\t4\t4");
        Mockito.verify(printStream).print("4\t6\t6");
        Mockito.verify(printStream).print("5\t8\t8");
    }
}
