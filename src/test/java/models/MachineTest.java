package models;

import models.Machine;
import models.Move;
import models.Scores;
import org.junit.Assert;
import org.junit.Test;

public class MachineTest {
    Machine machine  = new Machine();

    @Test
    public void checkMachineGivesInstance() {
        Assert.assertNotNull(machine);
    }


    // models.Machine will calculate score, based on type of move passed
    @Test
    public void testMachineGivesScore() {
        Assert.assertNotNull(machine.calculateScore(Move.COOPERATE, Move.COOPERATE));
    }

    @Test
    public void testMachineGivesTWOForEachPlayerWhenBothPerformCooperateMove() {
        Scores scores = machine.calculateScore(Move.COOPERATE, Move.COOPERATE);
        Assert.assertEquals(2, scores.getPlayer1Score());
        Assert.assertEquals(2, scores.getPlayer2Score());
    }

    @Test
    public void testMachineGivesZeroWhenBothCheats() {
        Scores scores = machine.calculateScore(Move.CHEAT, Move.CHEAT);
        Assert.assertEquals(0, scores.getPlayer1Score());
        Assert.assertEquals(0, scores.getPlayer2Score());
    }

    @Test
    public void testMachineGivesMinusOneForMeAndThreeForOtherWhenOtherCheatsAndICooperate() {
        Scores scores = machine.calculateScore(Move.COOPERATE, Move.CHEAT);
        Assert.assertEquals(-1, scores.getPlayer1Score());
        Assert.assertEquals(3, scores.getPlayer2Score());
    }

    @Test
    public void testMachineGivesPlusThreeForMeAndMinusOneForOtherWhenOtherCoOperatesAndICheat() {
        Scores scores = machine.calculateScore(Move.CHEAT, Move.COOPERATE);
        Assert.assertEquals(3, scores.getPlayer1Score());
        Assert.assertEquals(-1, scores.getPlayer2Score());
    }


}
