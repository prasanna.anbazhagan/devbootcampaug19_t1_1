package models;

import models.playables.ConsoleMoveBehaviour;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


public class PlayerTest {

    class MockUserInput implements UserInput {

        private String mockInput;

        public void setMockInput(String mockInput) {
            this.mockInput = mockInput;
        }

        @Override
        public String getStringInput() {
            return mockInput;
        }
    }

    MockUserInput mockUserInput;
    Player player;

    @Before
    public void setUp() {
        mockUserInput = new MockUserInput();
        player = new Player(new ConsoleMoveBehaviour(mockUserInput));
    }



    @Test
    public void testPlayerGivesInstance() {
        assertNotNull(player);
    }

    @Test
    public void testPlayersReturnsMove() {
        mockUserInput.setMockInput("CHEAT");
        Assert.assertEquals(Move.CHEAT, player.getCurrentMove());
    }

    @Test
    public void testPlayerGivesCooperateMoveWhenUserInoutIsMockedToGiveCooperate() {
        mockUserInput.setMockInput("COOPERATE");
        Assert.assertEquals(Move.COOPERATE, player.getCurrentMove());
    }

    @Test
    public void testPlayerGivesCooperateMoveWhenUserInoutIsMockedToGiveCheat() {
        mockUserInput.setMockInput("CHEAT");
        Assert.assertEquals(Move.CHEAT, player.getCurrentMove());
    }

    @Test
    public void testPlayersReturnCorrectScore() {
        player.addScore(2);
        assertEquals(2, player.getScore());
    }

    @Test
    public void testConsolePlayerReturnMoveBasedOnInput() {
        Player consoleBehaviourPlayer = new Player(new ConsoleMoveBehaviour(mockUserInput));

        mockUserInput.setMockInput("CHEAT");

        assertEquals(consoleBehaviourPlayer.getCurrentMove(), Move.CHEAT);
    }

    @Test
    public void testGoodPlayerReturnAlwaysCooperateMove() {
        Player goodPlayer = new Player(()->Move.COOPERATE);

        assertEquals(goodPlayer.getCurrentMove(), Move.COOPERATE);
    }

    @Test
    public void testBadPlayerReturnAlwaysCooperateMove() {
        Player badPlayer = new Player(()->Move.CHEAT);

        assertEquals(badPlayer.getCurrentMove(), Move.CHEAT);
    }



}
