package models;

import models.playables.DetectiveBehaviour;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class DetectiveBehaviourTest {

    @Test
    public void shouldReturnInstanceOfCopyCatPlayer() {
        DetectiveBehaviour behaviour = new DetectiveBehaviour();
        Assert.assertNotNull(behaviour);
    }

    @Test
    public void shouldReturnCooperateCheatCooperateAndCooperate() {
        DetectiveBehaviour behaviour = new DetectiveBehaviour();
        Assert.assertEquals(Move.COOPERATE, behaviour.getCurrentMove());
        Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());
        Assert.assertEquals(Move.COOPERATE, behaviour.getCurrentMove());
        Assert.assertEquals(Move.COOPERATE, behaviour.getCurrentMove());
    }

    @Test
    public void shouldBecomeCopyCatIfOpponentReturnsCheat() {
        DetectiveBehaviour behaviour = new DetectiveBehaviour();
        Game game = Mockito.mock(Game.class);
        behaviour.getCurrentMove();
        behaviour.update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
        behaviour.getCurrentMove();
        behaviour.update(game, new Move[]{Move.CHEAT, Move.CHEAT});
        behaviour.getCurrentMove();
        behaviour.update(game, new Move[]{Move.COOPERATE, Move.CHEAT});
        behaviour.getCurrentMove();
        behaviour.update(game, new Move[]{Move.COOPERATE, Move.CHEAT});


        Assert.assertEquals(Move.CHEAT, behaviour.getCurrentMove());
        behaviour.update(game, new Move[]{Move.CHEAT, Move.COOPERATE});
        Assert.assertEquals(Move.COOPERATE, behaviour.getCurrentMove());
    }
}
