import models.*;
import models.playables.ConsoleMoveBehaviour;
import models.playables.CopyCatMoveBehaviour;
import models.playables.DetectiveBehaviour;

import java.io.PrintStream;

public class GameController {

    public static void main(String[] args) {

        //runGameWithConsolePlayers();
        //runGameWithBehaviourPlayers();
        runGame4();
    }

    private static void runGameWithConsolePlayers() {
        ScannerUserInput scannerUserInput = new ScannerUserInput();

        Game game = new Game(new Player(new ConsoleMoveBehaviour(scannerUserInput)), new Player(new ConsoleMoveBehaviour(scannerUserInput)), new Machine(), 2, new GameOutput() {
            @Override
            public void printString(String output) {
                System.out.println(output);
            }
        });

        game.play();
    }

    private static void runGameWithBehaviourPlayers() {

        Game game = new Game(new Player(() -> Move.COOPERATE), new Player(() -> Move.CHEAT), new Machine(), 5, new GameOutput() {
            @Override
            public void printString(String x) {
                System.out.println(x);
            }
        });

        game.play();
    }

    private static void runGame4() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour();
        Player detective = new Player(detectiveBehaviour);
        CopyCatMoveBehaviour copyCatMoveBehaviour = new CopyCatMoveBehaviour();
        Player copyCat = new Player(copyCatMoveBehaviour);
        Game game = new Game(copyCat, detective, new Machine(), 5, output -> System.out.println(output));
        game.addObserver(copyCatMoveBehaviour);
        game.addObserver(detectiveBehaviour);
        game.play();
    }
}
