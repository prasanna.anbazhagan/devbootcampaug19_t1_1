import models.UserInput;

import java.util.Scanner;

public class ScannerUserInput implements UserInput {

    private Scanner scanner;

    public ScannerUserInput() {
        scanner = new Scanner(System.in);
    }

    @Override
    public String getStringInput() {
        System.out.print("Please Enter input : ");
        String input = scanner.nextLine();
        System.out.println();
        return input;
    }
}
