package models;

public enum Move {


    COOPERATE("COOPERATE"),
    CHEAT("CHEAT");

    String name;

    Move(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
