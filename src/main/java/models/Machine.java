package models;

public class Machine {

    public Scores calculateScore(Move player1Move, Move player2Move) {
        if(player1Move ==  Move.COOPERATE && player2Move == Move.COOPERATE) {
            return new Scores(2,2);
        } else if (player1Move == Move.COOPERATE && player2Move == Move.CHEAT) {
            return new Scores(-1,3);
        } else if(player1Move == Move.CHEAT && player2Move == Move.COOPERATE) {
            return new Scores(3,-1);
        }


        return new Scores(0,0);
    }
}
