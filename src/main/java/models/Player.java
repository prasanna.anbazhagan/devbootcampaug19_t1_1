package models;

public class Player {

    private int score;

    private MoveBehaviour moveBehaviour;

    public Player(MoveBehaviour moveBehaviour) {
        this.moveBehaviour = moveBehaviour;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public int getScore() {
        return score;
    }

    public Move getCurrentMove() {
        return moveBehaviour.getCurrentMove();
    }
}
