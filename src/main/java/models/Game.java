package models;

import java.util.Observable;

public class Game extends Observable {
    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final int noOfGames;
    private final GameOutput gameOutput;

    public Game(Player player1, Player player2, Machine machine, int noOfGames, GameOutput gameOutput) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfGames = noOfGames;
        this.gameOutput = gameOutput;
    }

    public void play() {
        for (int i = 0; i < noOfGames; i++) {
            calculateAndStoreRoundScore(i);
        }
    }

    private void calculateAndStoreRoundScore(int round) {
        Move player1Move = player1.getCurrentMove();
        Move player2Move = player2.getCurrentMove();

        setChanged();
        notifyObservers(new Move[]{player1Move, player2Move});

        Scores scores = machine.calculateScore(player1Move, player2Move);
        player1.addScore(scores.getPlayer1Score());
        player2.addScore(scores.getPlayer2Score());

        gameOutput.printString(String.format("%d\t%d\t%d", round + 1, player1.getScore(), player2.getScore()));
    }
}
