package models.playables;

import models.MoveBehaviour;
import models.Move;
import models.UserInput;

public class ConsoleMoveBehaviour implements MoveBehaviour {

    private UserInput userInput;

    public ConsoleMoveBehaviour(UserInput userInput) {
        this.userInput = userInput;
    }

    @Override
    public Move getCurrentMove() {
        return Move.valueOf(userInput.getStringInput());
    }
}
