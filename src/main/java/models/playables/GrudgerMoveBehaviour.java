package models.playables;

import models.Game;
import models.Move;
import models.MoveBehaviour;

import java.util.Observable;
import java.util.Observer;

public class GrudgerMoveBehaviour implements MoveBehaviour, Observer {

    private boolean isSomeOneCheated;

    @Override
    public Move getCurrentMove() {
        return isSomeOneCheated ? Move.CHEAT : Move.COOPERATE;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Move[]) {
            Move[] lastMoves = (Move[]) arg;
            if (lastMoves[0] == Move.CHEAT || lastMoves[1] == Move.CHEAT) {
                isSomeOneCheated = true;
            }
        }
    }
}
