package models.playables;

import models.Move;
import models.MoveBehaviour;

import java.util.Observable;
import java.util.Observer;

public class DetectiveBehaviour implements MoveBehaviour, Observer {

    private final Move[] initialMoves = new Move[]{Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE};

    private int noOfActionsPerformed = 0;

    private boolean isCheated = false;

    private Move myPreviousMove = null;

    private MoveBehaviour changedBehaviour;

    @Override
    public Move getCurrentMove() {

        noOfActionsPerformed++;
        if (changedBehaviour != null) {
            return changedBehaviour.getCurrentMove();
        } else {
            Move move = initialMoves[noOfActionsPerformed - 1];
            myPreviousMove = move;
            return move;
        }
    }

    @Override
    public void update(Observable o, Object arg) {

        if (changedBehaviour != null && changedBehaviour instanceof Observer) {
            ((Observer) changedBehaviour).update(o, arg);
        } else {
            if (arg instanceof Move[]) {
                Move[] lastMoves = (Move[]) arg;

                Move opponentMove = getOpponentMove(lastMoves);

                if (opponentMove == Move.CHEAT) {
                    isCheated = true;
                }

                if (noOfActionsPerformed >= 4) {
                    changeBehaviour(opponentMove);
                }
            }
        }
    }

    private Move getOpponentMove(Move[] lastMoves) {
        if(lastMoves[0] == myPreviousMove) {
            return lastMoves[1];
        } else {
            return lastMoves[0];
        }
    }

    private void changeBehaviour(Move lastOpponentMove) {
        if (isCheated) {
            changedBehaviour = new CopyCatMoveBehaviour(lastOpponentMove);
        } else {
            changedBehaviour = () -> Move.CHEAT;
        }
    }
}
