package models.playables;

import models.Move;
import models.MoveBehaviour;

import java.util.Observable;
import java.util.Observer;

public class CopyCatMoveBehaviour implements MoveBehaviour, Observer {

    private Move nextMove = Move.COOPERATE;

    public CopyCatMoveBehaviour() {
    }

    public CopyCatMoveBehaviour(Move nextMove) {
        this.nextMove = nextMove;
    }

    @Override
    public Move getCurrentMove() {
        return nextMove;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Move[]) {
            Move[] lastMoves = (Move[]) arg;

            if (lastMoves.length == 2) {
                nextMove = (nextMove != lastMoves[0]) ? lastMoves[0] : lastMoves[1];
            }
        }
    }
}
